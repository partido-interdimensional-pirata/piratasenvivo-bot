## setup en la ñufla

instalamos node, npm y python2 por que se necesita para compilar sqlite(!)
```
pacman -Sy nodejs npm python2
```
creamos usuarix y entramos
```
useradd -m renemontes
su renemontes
cd
```
conseguimos el código
```
git clone https://0xacab.org/pip/piratasenvivo-bot.git
cd piratasenvivo-bot
```
instalamos dependencias
```
npm install
```
creamos lx cyborg con [@BotFather](https://t.me/BotFather):
```
/newbot
Nombre de lx cyborg
NombreDeLxCyborg_bot
/setcommands
anunciar - Anunciar un mensaje a todxs lxs usuarixs
```
creamos servicio
```
exit
nano /etc/systemd/system/piratasenvivo.service
```
```service
[Unit]
Description=Rene Montes
After=network.target

[Service]
User=renemontes
Environment=CHAT_ID=<LA ID DE EL CHAT QUE MANEJA LX CYBORG>
Environment=TELEGRAM_TOKEN=<EL TOKEN DE LX CYBORG, DE BOTFATHER>
ExecStart=/usr/bin/node /home/renemontes/piratasenvivo-bot/index.js
WorkingDirectory=/home/renemontes/piratasenvivo-bot
Restart=on-failure
SuccessExitStatus=3 4
RestartForceExitStatus=3 4

[Install]
WantedBy=multi-user.target
```
lo activamos
```
systemctl enable --now piratasenvivo
```
